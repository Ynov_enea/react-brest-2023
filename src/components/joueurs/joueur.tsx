import axios from "axios";
import { useEffect, useState } from "react";
import { Grid, Typography } from "@mui/material";
import Theme from "../__global/theme/theme";

const Joueurs = () => {
  const joueursArray: String[] = [];
  const [joueurs, setJoueurs] = useState(joueursArray);

  // URL complète avec le token API
  const apiUrl = "/api/standings";

  useEffect(() => {
    axios
      .get(apiUrl)
      .then((res) => {
        setJoueurs(res.data);
      })
      .catch((error) => {
        console.error("Erreur de requête:", error.message);
      });
  }, []);

  return (
    <Theme>
      <Grid>
        {joueurs.map((j: any) => (
          <Grid container xs={2} key={j.id}>
            <Grid item>
              <Typography>{j.firstname}</Typography>
              <Typography>{j.lastname}</Typography>
            </Grid>
          </Grid>
        ))}
      </Grid>
    </Theme>
  );
};

export default Joueurs;
