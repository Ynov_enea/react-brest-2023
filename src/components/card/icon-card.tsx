import { Grid } from "@mui/material";

const IconCard = () => {
  return (
    <Grid>
      <img src="/assets/header_logo.svg" alt="logo" style={{ width: 60 }}></img>
    </Grid>
  );
};

export default IconCard;
