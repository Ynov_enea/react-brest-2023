import React from "react";
import { Box, Grid } from "@mui/material";
import Body from "../body/body";
import HeaderBar from "../../header-bar/header-bar.component";
import { ThemeProps } from "./theme.constant";
import { ThemeStyle } from "./theme.style";

const Theme = (props: React.PropsWithChildren<ThemeProps>) => {
  // Style
  const { MuiBox } = ThemeStyle;

  // Props
  const { children } = props;

  return (
    <Box style={MuiBox}>
      <Grid item>
        <HeaderBar />
      </Grid>
      <Grid item>
        <Body>{children}</Body>
      </Grid>
    </Box>
  );
};

export default Theme;
