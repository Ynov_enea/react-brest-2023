import { Grid } from "@mui/material";
import { BodyProps } from "./body.constant";
import { PropsWithChildren } from "react";
import { BodyStyle } from "./body.style";

const Body = (props: PropsWithChildren<BodyProps>) => {
  // Style
  const { MuiImage, MuiGrid, MuiGridContainer } = BodyStyle;

  const { children } = props;

  return (
    <Grid container sx={MuiGridContainer}>
      <Grid item xs={12} sx={MuiGrid}>
        <div style={MuiImage}>{children}</div>
      </Grid>
    </Grid>
  );
};

export default Body;
