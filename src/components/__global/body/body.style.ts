export const BodyStyle = {
  MuiGridContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 3,
  },
  MuiGrid: {
    justifyContent: "center",
    alignItems: "center",
  },
  MuiImage: {
    alignSelf: "center",
    height: "800px",
    borderRadius: 40,
    backgroundImage: "url('./assets/background.jpg')",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    display: "flex",
  },
};
