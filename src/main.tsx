import React from "react";
import ReactDOM from "react-dom/client";
import Accueil from "./components/accueil/accueil.component";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <Accueil />
  </React.StrictMode>
);
