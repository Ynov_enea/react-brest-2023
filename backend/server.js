const express = require("express");
const app = express();
const port = 5000;
import { API_TOKEN, API_BASE_URL } from "../../../constant.ts";

app.use(express.json());

app.get("/api/joueurs", async (req, res) => {
  try {
    const apiUrl = `${API_BASE_URL}/standings/rounds?api_token=${API_TOKEN}`;
    const response = await axios.get(apiUrl);
    res.json(response.data);
  } catch (error) {
    console.error("Erreur de requête:", error.message);
    res.status(500).json({ error: "Erreur de serveur interne" });
  }
});

app.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur le port ${port}`);
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
